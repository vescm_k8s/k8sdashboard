### bootstrap it
```sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
```

### create user
```sh
kubectl apply -f admin.yaml
```

### create cluster role binding
```sh
kubectl apply -f clusterRoleBind.yaml
```

### get admin admin user token and describe it's secret
```sh
kubectl -n kubernetes-dashboard get secret
kubectl -n kubernetes-dashboard describe secret $secret
```

### start the kube proxy
```sh
kubectl proxy
```

### access the dashboard URL and use the token
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=default

### if you want to clean up
```sh
kubectl delete -f admin.yaml
kubectl delete -f clusterRoleBind.yaml
```

#### sources
https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md

